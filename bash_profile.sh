source ~/bin/variables.bash.inc
source ~/bin/bootstrap.bash.inc

bashProfileRefresh () {
mkdir -p ~/bin
    mv -f ~/bin/methods.bash.inc ~/.Trash/
    curl -sS https://raw.githubusercontent.com/carlosmarte/my-personal-notes/master/mac-bash-profile.sh > ~/bin/methods.bash.inc
source ~/.bash_profile
    echo 'done'
}

source ~/bin/methods.bash.inc

# The next line updates PATH for the Google Cloud SDK.
source ~/SDK/google-cloud-sdk/path.bash.inc

# The next line enables bash completion for gcloud.
source ~/SDK/google-cloud-sdk/completion.bash.inc

source /usr/local/opt/chruby/share/chruby/chruby.sh